package datamanagement;

public class Unit implements IUnit {
    private String unitCode_;//uc
    private String unitName_;//UN
    private float psCutoff_;//co2
    private float crCutoff_;//co1
    private float diCutoff_;//co4
    private float hdCutoff_;//co3
    private float aeCutoff_;//co5
    private int asg1Weight_, asg2Weight_, examWeight_;//a1,a2,ex

    private final StudentUnitRecordList records;//rs

    public Unit(String uc, String un, float f1, float f2, float f3, float f4,
            float f5, int i1, int i2, int i3, StudentUnitRecordList rl) {

        this.unitCode_ = uc;
        this.unitName_ = un;
        this.setCutoffs( f1, f2, f3, f4, f5);
        this.setAssessmentWeights(i1, i2, i3);

        if(rl == null){
            records = new StudentUnitRecordList();
        } else {
            records = rl;
        }
    }

    @Override
    public String getUnitCode() {
        return this.unitCode_;
    }

    @Override
    public String getUnitName() {
        return this.unitName_;
    }

    @Override
    public void setPsCutoff(float cutoff) {
        this.psCutoff_ = cutoff;
    }

    @Override
    public float getPsCutoff() {
        return this.psCutoff_;
    }

    @Override
    public void setCrCutoff(float cutoff) {
        this.crCutoff_ = cutoff;
    }

    @Override
    public float getCrCutoff() {
        return this.crCutoff_;
    }

    @Override
    public void setDiCutoff(float cutoff) {
        this.diCutoff_ = cutoff;
    }

    @Override
    public float getDiCuttoff() {
        return this.diCutoff_;
    }
    //pointless function? check dependency
    /*public void HDCutoff(float cutoff) {
        this.hdCutoff_ = cutoff;
    }*/

    @Override
    public void setHdCutoff(float cutoff) {
        this.hdCutoff_ = cutoff;
    }

    @Override
    public float getHdCutoff() {
        return this.hdCutoff_;
    }

    @Override
    public void setAeCutoff(float cutoff) {
        this.aeCutoff_ = cutoff;
    }

    @Override
    public float getAeCutoff() {
        return this.aeCutoff_;
    }

    @Override
    public void addStudentRecord(IStudentUnitRecord record) {
        records.add(record);
    }

    @Override
    public IStudentUnitRecord getStudentRecord(int studentID) {
        for (IStudentUnitRecord r : records) {
            if (r.getStudentID() == studentID)
                return r;
        }
        return null;
    }

    @Override
    public StudentUnitRecordList listStudentRecords() {
        return records;
    }

    @Override
    public int getAsg1Weight() {
        return asg1Weight_;
    }

    @Override
    public int getAsg2Weight() {
        return asg2Weight_;
    }

    @Override
    public int getExamWeight() {
        return examWeight_;
    }

    @Override
    public final void setAssessmentWeights(int a1, int a2, int ex) {
        if (a1 < 0 || a1 > 100 ||
            a2 < 0 || a2 > 100 ||
            ex < 0 || ex > 100 ) {
            throw new RuntimeException("Assessment weights cant be less than "
                    + "zero or greater than 100");
        }
        if (a1 + a2 + ex != 100) {
            throw new RuntimeException("Assessment weights must add to 100");
        }
        this.asg1Weight_ = a1;
        this.asg2Weight_ = a2;
        this.examWeight_ = ex;
    }

    private void setCutoffs( float ps, float cr, float di, float hd, float ae){
        if (ps < 0 || ps > 100 ||
            cr < 0 || cr > 100 ||
            di < 0 || di > 100 ||
            hd < 0 || hd > 100 ||
            ae < 0 || ae > 100 ) {
            throw new RuntimeException("Assessment cutoffs cant be less than "
                    + "zero or greater than 100");
        }
        if (ae >= ps) {
            throw new RuntimeException("AE cutoff must be less than PS cutoff");
        }
        if (ps >= cr) {
            throw new RuntimeException("PS cutoff must be less than CR cutoff");
        }
        if (cr >= di) {
            throw new RuntimeException("CR cutoff must be less than DI cutoff");
        }
        if (di >= hd) {
            throw new RuntimeException("DI cutoff must be less than HD cutoff");
        }
        this.psCutoff_ = ps;
        this.crCutoff_ = cr;
        this.diCutoff_ = di;
        this.hdCutoff_ = hd;
        this.aeCutoff_ = ae;

    }

    public String getGrade(float f1, float f2, float f3) {
        float totalMarks = f1 + f2 + f3;

        if (f1 < 0 || f1 > asg1Weight_ ||
            f2 < 0 || f2 > asg2Weight_ ||
            f3 < 0 || f3 > examWeight_ ) {
            throw new RuntimeException("marks cannot be less than zero or "
                    + "greater than assessment weights");
        }

        if (totalMarks < aeCutoff_) {
            return "FL";
        }
        else if (totalMarks < psCutoff_) {
            return "AE";
        }
        else if (totalMarks < crCutoff_) {
            return "PS";
        }
        else if (totalMarks < diCutoff_) {
            return "CR";
        }
        else if (totalMarks < hdCutoff_) {
            return "DI";
        }
        else{
            return "HD";
        }
    }
}
