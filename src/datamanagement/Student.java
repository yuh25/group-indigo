package datamanagement;

public class Student implements IStudent {
    private Integer id_; 
    private String fName_;
    private String lName_;
    private StudentUnitRecordList unitRecord_;

    public Student( Integer id, String fn, String ln, StudentUnitRecordList su ) { 
        this.id_ = id; 
		this.fName_ = fn;
        this.lName_ = ln;
		this.unitRecord_ = su == null ? new StudentUnitRecordList() : su;
    }

    public Integer getID() { 
        return this.id_; 
    } 

    public String getFirstName() { 
        return this.fName_; 
    }

    public void setFirstName(String firstName) { 
        this.fName_ = firstName; 
    }

    public String getLastName() { 
        return this.lName_; 
    }

    public void setLastName(String lastName) {  
		this.lName_ = lastName; 
    }

    public void addUnitRecord(IStudentUnitRecord record) { 
        unitRecord_.add(record); 
    }

    public IStudentUnitRecord getUnitRecord( String unitCode ) {
        for ( IStudentUnitRecord r : unitRecord_ ) 
            if ( r.getUnitCode().equals(unitCode)) return r; 
            return null;      
    }

    public StudentUnitRecordList getUnitRecords() { 
        return this.unitRecord_; 
    }

}