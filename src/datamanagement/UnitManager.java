package datamanagement;

import static java.lang.Float.parseFloat;
import static java.lang.Integer.parseInt;
import java.util.List;
import org.jdom.*;

public class UnitManager {

    private static UnitManager self = null;

    private UnitMap unitMap_;

    public static UnitManager UM() {
        if (self == null){
                self = new UnitManager();
        }
        return self;
    }

    private UnitManager() {
        unitMap_ = new UnitMap();
    }

    public IUnit getUnit(String uc) {
        IUnit iUnit = unitMap_.get(uc);
        return iUnit != null ? iUnit : createUnit(uc);
    }

    private IUnit createUnit(String unitCode) {

        IUnit iUnit;
        List<Element> temp = XMLManager.getXML().getDocument().getRootElement().getChild("unitTable").getChildren("unit");

        for (Element el : temp)
            if (unitCode.equals(el.getAttributeValue("uid"))) {
                iUnit = new Unit(   el.getAttributeValue("uid"),
                                    el.getAttributeValue("name"),
                                    parseFloat(el.getAttributeValue("ps")),
                                    parseFloat(el.getAttributeValue("cr")),
                                    parseFloat(el.getAttributeValue("di")),
                                    parseFloat(el.getAttributeValue("hd")),
                                    parseFloat(el.getAttributeValue("ae")),
                                    parseInt(el.getAttributeValue("asg1wgt")),
                                    parseInt(el.getAttributeValue("asg2wgt")),
                                    parseInt(el.getAttributeValue("examwgt")),
                                    StudentUnitRecordManager.instance().getRecordsByUnit(unitCode));

                unitMap_.put(iUnit.getUnitCode(), iUnit);
                return iUnit;
                }

        throw new RuntimeException("DBMD: createUnit : unit not in file");
    }

    public UnitMap getUnits() {
        UnitMap unitMap;
        IUnit iUnit;

        unitMap = new UnitMap();
        for (Element el : (List<Element>)XMLManager.getXML().getDocument().getRootElement().getChild("unitTable").getChildren("unit")) {
            iUnit = new UnitProxy(  el.getAttributeValue("uid"),
                                    el.getAttributeValue("name")
            );
            unitMap.put(iUnit.getUnitCode(), iUnit);
        } // unit maps are filled with PROXY units
        return unitMap;
    }
}
