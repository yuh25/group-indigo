package datamanagement;

public class StudentProxy implements IStudent {
    private Integer id_;
    private String fName_;
    private String lName_;
    private StudentManager stuMan_;

    public StudentProxy(Integer id, String fn, String ln) {
        this.id_ = id;
        this.fName_ = fn;
        this.lName_ = ln;
        this.stuMan_ = StudentManager.get();
    }

    public Integer getID() { 
        return id_;    
    }
    public String getFirstName() { 
        return fName_; 
    }

    public String getLastName() { 
        return lName_; 
    }
    public void setFirstName(String firstName) {
        stuMan_.getStudent(id_).setFirstName(firstName);
    }

    public void setLastName(String lastName) {
        stuMan_.getStudent(id_).setLastName(lastName);
    }

    public void addUnitRecord(IStudentUnitRecord record) {
        stuMan_.getStudent(id_).addUnitRecord(record);
    }

    public IStudentUnitRecord getUnitRecord(String unitCode) {
        return stuMan_.getStudent(id_).getUnitRecord(unitCode);
    }

    public StudentUnitRecordList getUnitRecords() {
         return stuMan_.getStudent(id_).getUnitRecords();
    }

}
