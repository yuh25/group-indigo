package datamanagement;

import org.jdom.*;
import java.util.List;

public class StudentManager {
    private static StudentManager self_ = null;
    private StudentMap sMap_;
    private java.util.HashMap<String, StudentMap> hashMap_;
    
    public static StudentManager get() {
        if (self_ == null)       
            self_ = new StudentManager(); 
        return self_; 
    }

    private StudentManager() {
        sMap_ = new StudentMap();
        hashMap_ = new java.util.HashMap<>();
    }

    public IStudent getStudent(Integer id) {
        IStudent iStudent = sMap_.get(id);
        return iStudent != null ? iStudent : createStudent(id);
    }

    private Element getStudentElement(Integer id) {
            for (Element el : (List<Element>) XMLManager.getXML().getDocument().getRootElement().getChild("studentTable").getChildren("student")) 
                if (id.toString().equals(el.getAttributeValue("sid"))) 
                    return el;
                return null;
    }

    private IStudent createStudent(Integer id) {
        IStudent iStudent;
        Element studentElement = getStudentElement(id);
        if (studentElement != null) {
            StudentUnitRecordList rlist = StudentUnitRecordManager.instance().getRecordsByStudent(id);
            iStudent = new Student(new Integer(studentElement.getAttributeValue("sid")),studentElement.getAttributeValue("fname"),studentElement.getAttributeValue("lname"),rlist);
            sMap_.put(iStudent.getID(), iStudent);
            return iStudent; 
        }
        throw new RuntimeException("DBMD: createStudent : student not in file");
    }

    private IStudent createStudentProxy(Integer id) {
        Element studentElement = getStudentElement(id);
        if (studentElement != null) return new StudentProxy(id, studentElement.getAttributeValue("fname"), studentElement.getAttributeValue("lname"));
            throw new RuntimeException("DBMD: createStudent : student not in file");
    }

    public StudentMap getStudentsByUnit(String unit) {
        StudentMap studentMap = hashMap_.get(unit);
        if (studentMap != null) {
            return studentMap;
        }
        studentMap = new StudentMap();
        IStudent iStudent;
        StudentUnitRecordList unitRecord = StudentUnitRecordManager.instance().getRecordsByUnit(unit);
        for (IStudentUnitRecord S : unitRecord) {
            iStudent = createStudentProxy(new Integer(S.getStudentID()));
            s.put(iStudent.getID(), iStudent);
        }
        um_.put( unit, studentMap);
        return studentMap;
    }

}