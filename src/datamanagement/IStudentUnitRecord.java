package datamanagement;

public interface IStudentUnitRecord {

	public float getExam();
    public float getTotal();
    public float getAsg1();
    public float getAsg2(); 
    public Integer getStudentID();
    public String getUnitCode();

    public void setAsg1(float mark);

    public void setAsg2(float mark);

    public void setExam(float mark);
}
