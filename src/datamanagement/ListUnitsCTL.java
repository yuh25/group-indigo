package datamanagement;

public class ListUnitsCTL {
    private UnitManager unitManager_;

    public ListUnitsCTL() {
        unitManager_ = UnitManager.UM();
    }

    public void listUnits(IUnitLister iUnitLister) {
        iUnitLister.clearUnits();
        UnitMap units = unitManager_.getUnits();
        for (String s : units.keySet()){
            iUnitLister.addUnit(units.get(s));
        }
    }
}
